# Awesome-Display-Setting project

## Dependencies

- Lua-5.3
- Lua53-lgi
- lua53-filesystem

## What is it

Setting display screen position and own setting to command at the final time "xrandr".
I do start it to learn Lua at first place and start it from Awesome WM.
I do discover on the same project lgi wrapper for Gtk+ GUI.
So it is a kind of nice challenge to me and i really appreciate help from IRC channels: #lua and #gtk+
(lot of nice help from RhodiumToad without i would not be able to do it so quickly and so easily at first step)

## Screenshot (because an image show more than words can do)

![application](images/application_screenshot.jpg)

## How does code works ?

The design is now an MVC design at all step.
There is some concern scoped to split by functionality:

- main.lua is in charge to control and run the application. just make it executable to use it the easy way.
- views.main_window module to draw the main window UI from a Glade XML file
- screen_shapes [model, view, controller] to target draw and move of displays screens under the tool bar of main window (one model and one view for each display screen item to draw and make moving, and one controller to manage them)
- screen_settings [model, view, controller] to target on selected display screen setting to include/select in widgets entries under the canvas shapes on left side.
- screen_records (also MVC designed) care about records of settings profiles (right of settings area).
- There is a special model to use for extract data from "xrandr" command: models.xrandr_extractor does it.
  
## Licence

[GPL-v3.0](gpl-3.0.md)
