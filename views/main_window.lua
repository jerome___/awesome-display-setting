------------------------------------------------------------
-- Draw main Window GTK+3 view from Glade XML file source
------------------------------------------------------------

local lgi = require 'lgi'
local lfs = require 'lfs'
local config = require 'models.config'


local Gtk = lgi.require('Gtk', '3.0')
-- local Gtk = lgi.Gtk
local Goo = lgi.require('GooCanvas')
local GdkPixbuf = lgi.require('GdkPixbuf')

local assert = lgi.assert
local ui, window, tool_bar, status_bar, goo_canvas

local _MAIN_WINDOW = {}

function _MAIN_WINDOW.build(self)
  -- Draw the main UI, get the containers already defined
  local builder = Gtk.Builder()
  local current_dir = io.popen"cd":read'*l' 
  assert(builder:add_from_file(lfs.currentdir()..'/models/ui_gtk_display_setting.glade'))
  -- assert(builder:add_from_file(current_dir..'/models/ui_gtk_display_setting.glade'))
  ui = builder.objects  -- ui contain all the objects builded from Glade XML file model source
  window = ui.main_window  -- Main window container extraction
  -- events on Main Window handlers
  window.on_key_press_event = nil
  window.on_map_event = function() window.on_map_event = nil end
  window.on_destroy = Gtk.main_quit
  -- GooCanvas container build from Goo factory Canvas
  goo_canvas = Goo.Canvas { parent = ui.screen_position , id = 'canvas',
                            width = config.canvas.width,
                            height = config.canvas.height }
  -- tool bar and status bar extraction from ui
  status_bar = ui.status_bar
  tool_bar = ui.tool_bar
  -- Build tool bar buttons for About and Quit functionnalities
  local about_button =  Gtk.ToolButton { stock_id = 'gtk-about' }
  function about_button:on_clicked()
     local dlg = Gtk.AboutDialog {
        program_name = 'Awesome display settings',
        title = 'About...',
        name = "Config display's screens.\nBuild with Lua and Gtk+ (lgi)",
        copyright = '(C) Copyright 2021, Licence GPL-3.0',
        authors = { 'Jerome Lanteri', },
        version = "0.2 rev 05",
        website = "https://bitbucket.org/jerome___/awesome-display-setting/",
        website_label = "Source code url",
        logo = GdkPixbuf.Pixbuf.new_from_file(
            lfs.currentdir().."/images/awesome_display_setting_icon_(48x48).png")
            --current_dir.."/images/awesome_display_setting_icon_(48x48).png")
     }
     if tonumber(Gtk._version) >= 3 then dlg.license_type = Gtk.License.GPL_3_0 end
     dlg:run()
     dlg:hide()
  end
  -- Tool bar content set
  tool_bar:insert(Gtk.ToolButton { stock_id = 'gtk-quit', on_clicked = function() window:destroy() end }, -1)
  tool_bar:insert(about_button, -1)
  -- Status bar content set
  local ctx = status_bar:get_context_id('default')
  status_bar:push(ctx, 'This is status_bar message.')
  return ui, window, goo_canvas
end

function _MAIN_WINDOW.get_ui(self)
    return ui
end
function _MAIN_WINDOW.get_window(self)
    return window
end

function _MAIN_WINDOW.get_screen_canvas(self)
    return goo_canvas
end

return _MAIN_WINDOW
