---------------------------------------------
-- View for Setting screen widget 
---------------------------------------------

local lgi = require 'lgi'
local Gtk = lgi.Gtk

local _SETTING_VIEW = {}
local meta = {}
local ui = {}


function _SETTING_VIEW.new(self, ui_setup, ref, model)
    ui = ui_setup
    self.widget = ui[ref] -- .screen_display
    if type(model) == 'table' then
        self.widget.cells = {{ Gtk.CellRendererText(), {text = 1}, align = 'start' }}
        self.widget:set_model(model.form)
    end
    return setmetatable({ widget = self.widget }, meta)
end

function _SETTING_VIEW.update_model(self, model)
    self.widget:set_model(model.form)
end

meta.__index = _SETTING_VIEW

return _SETTING_VIEW
