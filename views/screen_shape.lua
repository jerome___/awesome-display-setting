-----------------------------------------------------------------------------------
-- Build GooCanvas view to draw display/screen devices wire-connected to computer
-- this model is a fake class by using meta table __index
-----------------------------------------------------------------------------------

local lgi = require 'lgi'
local Goo = lgi.GooCanvas
local config = require 'models.config'

local _SCREEN_DISPLAY = {}
local meta = {}

function _SCREEN_DISPLAY.new(self, model, canvas)
    self.item = canvas:get_item(model.root)
    return setmetatable({ item = self.item }, meta)
end

meta.__index = _SCREEN_DISPLAY

return _SCREEN_DISPLAY
