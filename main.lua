#!/bin/env lua

--------------------------------------------------------------------------------
--         File:  awesome_display_setting.lua
--
--        Usage:  ./awesome_dsipaly_setting.lua
--
--  Description: Setting for screen with GooCanvas toolkit GUI
--
--      Options:  should get UI from glade XML file
-- Requirements:  lua53-lgi, lu53-lfs, lua5.3
--         Bugs:  nothing, but not finished
--        Notes:  with the great help of RhodiumToad from IRC #lua channel to start with lgi
--       Author:  (Jerome Lanteri), <jerome.archlinux@gmail.com>
-- Organization:  
--      Version:  0.2
--      Created:  22/12/2020
--     Revision:  5
--------------------------------------------------------------------------------
local lgi = require 'lgi'
local config = require 'models.config'
local xrandr = require 'models.xrandr_extractor'
local main_window = require "views.main_window"
local devices_controller = require 'controllers.screen_shapes'
local settings_controller = require 'controllers.screen_settings'
local test_settings = require 'controllers.test_settings'

local Gtk = lgi.Gtk
local ui, win, canvas, _xrandr

local function mouse_released_item_shape(item, t, event, name)
  -- When display screen item's shape has been released, do:
  --   update display name selected setting ComboBox 
  --   (this will serialize other events  actions)
  local index
  local i = 0
  for _name, _ in pairs(_xrandr.settings) do
    if name == _name then index = i; goto end_loop; end
    i = i + 1
  end; ::end_loop::
  if index then ui.screen_display:set_active(index) end
end

-- Start Application actions --

_xrandr = xrandr:new()                         -- screens settings and options (xrandr)
ui, win, canvas = main_window:build()          -- Draw the UI from Glade XML file
local devices_shapes = devices_controller:build(canvas, _xrandr, mouse_released_item_shape)
local settings = settings_controller:new(ui, _xrandr, devices_shapes)              -- settings widgets screen selected
local test = test_settings:new(ui, settings, config.testing.remaining_time)

-- Show GUI and start Gtk loop
win:show_all()
Gtk.main()
