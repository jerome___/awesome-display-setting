----------------------------------------------------------------
-- Model package to holde Display Devices view items settings
-- this model is a fake class by using meta table __index
----------------------------------------------------------------

local _MODEL_SCREEN = {}
local meta = {}
local lgi = require 'lgi'
local Goo = lgi.GooCanvas
local config = require 'models.config'

function _MODEL_SCREEN.new(self, _name, settings, scale, root_model)
    self.scale = scale
    self.name = _name
    self.size = { width = math.floor(settings.size:match("^(%d+)x%d+") * scale),
                  height = math.floor(settings.size:match("^%d+x(%d+)") * scale) }
    self.position = { x = math.floor(settings.position:match("^%+(%d+)%+%d+") * scale),
                      y = math.floor(settings.position:match("^%+%d+%+(%d+)") * scale) }
    self.primary = settings.primary
    self.root = Goo.CanvasGroupModel {
        x = self.position.x, y = self.position.y,
        width = self.size.width, height = self.size.height }
    self.rectangle = Goo.CanvasRectModel {
        line_width = 2, x = 0, y = 0,
        width = self.size.width, height = self.size.height,
        stroke_color = config.canvas.color.unselected_border,
        fill_color = self.primary and
                     config.canvas.color.primary_bg or
                     config.canvas.color.normal_bg,
        radius_x = 100 * scale, radius_y = 100 * scale }
    self.text = Goo.CanvasTextModel {
        text = _name,
        x = self.size.width / 2 - 20,
        y = self.size.height / 2 - 8,
        width = -1,
        anchor = Goo.GOO_CANVAS_ANCHOR_CENTER,
        font = "source-code bold 12",
        fill_color = self.primary and
                     config.canvas.color.primary_txt or
                     config.canvas.color.normal_txt }
    self.root:add_child(self.rectangle, -1)
    self.root:add_child(self.text, -1)
    root_model:add_child(self.root, -1)
    return setmetatable({ root = self.root,
                          rectangle = self.rectangle,
                          text = self.text,
                          name = self.name,
                          size = self.size,
                          position = self.position,
                          primary = self.primary,
                          scale = self.scale,
                         }, meta)
end

function _MODEL_SCREEN.set_position(self, _x, _y)
    self.position = { x = _x, y = _y }
    self.root.x = _x
    self.root.y = _y
end

meta.__index = _MODEL_SCREEN

return _MODEL_SCREEN
