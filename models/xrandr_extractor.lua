---------------------------------------------------------
-- Extract data from "xrandr" Linux/Unix CLI Command
--   return for each screen:
--     _ actual settings 
--     _ options
---------------------------------------------------------

local _XRANDR = {}
local meta = {}

local function define_line_type(line)
  -- return which kind of line entry is between:
  -- [connected, disconnected, inside, virtual]
  return line:match("^[%w-]+ (connected) ") or 
                    line:match("^[%w-]+ (disconnected)") or 
                    (line:match("^   (%d+x%d+) ") and "inside") or
                    "virtual"
end

local function extract_settings(line)
  -- extract from xrandr line iterated:
  --   _ "connected" line content matched value as: <bool>
  --   _ flag string possibly: ["connected" | "inside" | "disconnected" | "virtual"]
  --   _ current connected setting representation: [{...} | false].
  local screen = { name      = line:match("^([%w-]+) connected ") ,
                   enable    = line:match(" connected") and true or false,
                   primary   = string.match(line, ".*primary") and true or false,
                   size      = line:match("^[%w-]+ connected [primary ]*(%d+x%d+)"),
                   position  = line:match("^[%w-]+ connected [primary ]*%d+x%d+(%+%d+%+%d+)"),
                   rotate    = line:match(".*%d (left).*") or
                               line:match(".*%d (right)") or
                               line:match(".*%d (inverted).*") or "normal",
                   reflect   = line:match(".* (X axis) .*") or
                               line:match(".* (Y axis) .*") or
                               line:match(".* (X and Y axis) .*") or "",
                   dimension = line:match(".*%)%s(.*)") }
  return screen.name, screen
end

local function extract_options(line)
  -- extract from xrandr current iterated line, options oif connected screen
  --  _ "inside" matched flag: <bool>
  --  _ options representation for a size and some frequencies options as: 
  --        { size = "<size>", options = {...}, selected = <freq> }
  local _frequencies = {} 
  local _tag, _size, _freq
  local _selected = "_"
  local i = 1
  for word in string.gmatch(line, "([^%s]+)") do
    if i == 1 then _size = word:match("^(%d+x%d+)$")
    else 
      _freq, _tag = word:match("^(%d+%.%d+)(%*?%+?)")
      if _freq then _frequencies[#_frequencies + 1] = _freq end
      if _tag == "*+" then _selected = i - 1 end
    end
    i = i + 1
  end
  return _selected, {size = _size, freq = _frequencies}
end

function _XRANDR.new(self)
  -- Return xrandr screen data current setting and options result to be used
  local screens_settings = {}
  local screens_options = {}
  local xrandr = io.popen("xrandr")
  local name
  if xrandr then
    local options = {}
    for line in xrandr:lines() do
      local flag = define_line_type(line)
      if flag == 'connected' then
        local settings
        name, settings = extract_settings(line)
        screens_settings[name] = settings
        options = {}
      elseif flag == 'inside' then
        local index = #options + 1
        local _selected, _options = extract_options(line)
        options[index] = { selected = _selected, size = _options.size, freq = _options.freq }
      elseif flag == 'virtual' then name = nil
      elseif flag == 'disconnected' then name = nil end
      if name then screens_options[name] = options end
    end
    xrandr:close()
  end
  return setmetatable( {options = screens_options, settings = screens_settings}, meta)
end

meta.__index = _XRANDR

return _XRANDR

--[[
{
  options = {
    <string> = {	--? (name)
      <number> = {
        selected = <string|number>, --? ["_" | (freq.index)]
        size = <string>,
        freq = { <number> = <string> }
      }
    }
  },
  settings = {
    <string> = {  --? name
      enable = <boolean>,
      primary = <boolean>,
      size = <string>,
      position = <string>,
      rotate = <string>,
      reflect = <string>,
      dimension = <string>
    }
  }
}
--]]