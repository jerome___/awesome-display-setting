---------------------------------------------
-- Model for Setting screen widget 
---------------------------------------------

local lgi = require 'lgi'
local Gtk = lgi.Gtk
local GObject = lgi.GObject

local _SETTING_MODEL = {}
local meta = {}


function _SETTING_MODEL.new(self, _type, length)
    self.model = {}
    if _type == "STRING" then
        if length == 2 then
          self.model = Gtk.ListStore.new { [1] = GObject.Type.STRING,
                                           [2] = GObject.Type.STRING }
        elseif length == 1 then
          self.model = Gtk.ListStore.new { [1] = GObject.Type.STRING }
        end
    end
    return setmetatable({ form = self.model }, meta)
end

meta.__index = _SETTING_MODEL

return _SETTING_MODEL
