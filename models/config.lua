------------------------------------------------------
-- Share config data there with anything as a model
------------------------------------------------------

local _CONFIG = {
    canvas = {
        width = 400,
        height = 400,
        color = {
            selected_border = "red",
            unselected_border = "white",
            normal_bg = "blue",
            normal_txt = "white",
            primary_bg = "pink",
            primary_txt = "black",
            disabled_display = {    -- when display is disable
                calcul = "SUBSTRACT",    -- [ADD, SUBSTRACT, AND, OR]
                disable_bg = "#333333",
                disable_txt = "#333333",
                disable_border = "#333333"
            }
        }
    },
    settings = {
        changed_fg = "",
    },
    testing = {
        remaining_time = 10,
    },
    records = {
        selected_row_fg = "",
        selected_row_bg = ""
    }
}

return _CONFIG
