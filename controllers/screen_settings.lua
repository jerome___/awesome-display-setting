local xrandr_extractor = require "models.xrandr_extractor"
---------------------------------------------
-- Controller for Settings screens widgets 
---------------------------------------------

local lgi = require 'lgi'
local setting_model = require 'models.screen_settings'
local setting_view = require 'views.screen_settings'

local _SETTINGS_CONTROLLER = {}
local meta = {}
local Gtk = lgi.Gtk
local ui
local choices = {}

local function update_choices(name, key, value, xrandr)
    -- update choices and wash by remove unused or equal xrandr values
    local function extract_selected_freq()
      for i, option in ipairs(xrandr.options[name]) do
        if type(option.selected) == 'number' then return option.freq[option.selected + 1] end
      end
    end
    local value_to_compare = key ~= "frequency" and xrandr.settings[name][key] or extract_selected_freq()
    if choices[name]  ~= nil then  -- choice exist for this item
      if choices[name][key] ~= nil then -- item choice exist for this entry key
        -- and if value is the same than inital setting then remove this key
        if value_to_compare == value then choices[name][key] = nil
        -- else not same initial setting value for this item entry key => update value
        else choices[name][key] = value end
      -- else if item entry key choice empty and different initial setting value => create new key/value choice
      elseif value_to_compare ~= value then choices[name][key] = value end
      local length = 0
      for _, _ in pairs(choices[name]) do length = length + 1 end  -- is there something inside ?
      if length == 0 then choices[name] = nil end  -- nothing inside ==> can remove
    elseif value_to_compare ~= value then
      choices[name] = { [key] = value }
    end
end

local function build_full_setting(name, item_settings, initial_item_settings)
  for k, _ in pairs() do
    if item_settings[k] == nil then item_settings[k] = initial_item_settings[k] end
  end
end

function _SETTINGS_CONTROLLER.modify_model_item_to_move_on_axe(self, key, value, model)
  local scale = model.scale
  local pos_x = model.position.x
  local pos_y = model.position.y
  local width = model.size.width
  local height = model.size.height
  local center_x = (pos_x + width / 2) * scale
  local center_y = (pos_y + height / 2) * scale
  print("get center: ("..center_x..","..center_y..") for key = "..key)
  if key == "rotate" then
    print("get value: "..value)
    if value == "normal" then
      model.root:rotate(0, pos_x * scale, pos_y * scale)
    elseif value == "right" then 
      model.root:rotate(90, pos_x * scale, pos_y * scale)
      model.root:translate(0, - height * scale)
    elseif value == "left" then 
      model.root:rotate(270, center_x, center_y)
      model.root:translate(width * scale, height * scale)
    elseif value == "inverted" then 
      model.root:rotate(180, center_x, center_y)
      model.root:translate(0, width * scale)
    end
  elseif key == "reflect" then
    print("go for value: "..value)
    if value == "x" then
      model.root:scale(-1, 1)
      model.root:translate(((-2 *  pos_x) - width) * scale, 0)
      print("model x = "..model.root.x)
    end
    if value == "y" then
      model.root:scale(1, -1)
      model.root:translate(0, ((-2 * pos_y) - height) * scale)
    end
    if value == "xy" then
      model.root:scale(-1, -1)
      model.root:translate(((-2 *  pos_x) - width) * scale, ((-2 * pos_y) - height) * scale)
    end
  end
end

function _SETTINGS_CONTROLLER.new(self, ui_setup, _xrandr, shapes)
    ui = ui_setup
    self.xrandr = _xrandr
    self.shapes = shapes
    self.models = { name = setting_model:new("STRING", 1),
                    size = setting_model:new("STRING", 1),
                    frequency = setting_model:new("STRING", 1),
                    orientation = setting_model:new("STRING", 2),
                    symetry = setting_model:new("STRING", 2) }
    self.views = { name = setting_view:new(ui_setup, "screen_display", self.models.name),
                   is_primary = setting_view:new(ui_setup, "is_primary", false);
                   is_enable = setting_view:new(ui_setup, "is_enable", false);
                   size = setting_view:new(ui_setup, "screen_size", self.models.size),
                   frequency = setting_view:new(ui_setup, "screen_frequency", self.models.frequency),
                   orientation = setting_view:new(ui_setup, "screen_orientation", self.models.orientation),
                   symetry = setting_view:new(ui_setup, "screen_symetry", self.models.symetry) }
    self:update_widget_screen()
    -- EVENTS override to handle screen's display settings selection
    local function get_selected_item_name()
        local iter_name = ui.screen_display:get_active_iter()
        if iter_name then
            local model_name = ui.screen_display:get_model()
            return model_name[iter_name][1]    --  return the selected item name
        else return nil end
    end
    function ui.screen_display:on_changed()
        -- ComboBox name selection change:
        local index = ui.screen_display:get_active()
        local name
        local i = 0
        for _name, _ in pairs(_xrandr.settings) do
            if i == index then name = _name; goto end_loop; end
            i = i + 1
        end; ::end_loop::
        _SETTINGS_CONTROLLER:update_widget_is_primary(name)    -- is_primary Switch widget
        _SETTINGS_CONTROLLER:update_widget_is_enable(name)     -- is_enable Switch widget
        _SETTINGS_CONTROLLER:update_widget_size(name)           -- size ComboBox widget
        _SETTINGS_CONTROLLER:update_widget_orientation(name)   -- orientation ComboBox widget
        _SETTINGS_CONTROLLER:update_widget_symetry(name)       -- symetry ComboBox widget
    end
    function ui.is_primary:on_state_set()
        -- Switch state change for **is_primary** widget
        local name = get_selected_item_name()
        local primary = self:get_active()
        if name then update_choices(name, "primary", primary, _xrandr) end
    end
    function ui.is_enable:on_state_set()
        -- Switch state change for **is_enable** widget
        local name = get_selected_item_name()
        local enable = self:get_active()
        if name then update_choices(name, "enable", enable, _xrandr) end
    end
    function ui.screen_size:on_changed()
        -- ComboBox size selection change,
        -- update frequencies combobox content
        local iter = self:get_active_iter()
        local name = get_selected_item_name()
        if iter then local size = self.model[iter][1]
          if name then 
            update_choices(name, "size", size, _xrandr)
            _SETTINGS_CONTROLLER:update_widget_frequencies(name, size)
          end
        end
    end
    function ui.screen_frequency:on_changed()
        -- ComboBox frequency selection changed
        local iter = self:get_active_iter()
        local name = get_selected_item_name()
        if iter then local frequency =  self.model[iter][1]
          if name then update_choices(name, "frequency", frequency, _xrandr) end
        end
    end
    function ui.screen_orientation:on_changed()
        -- ComboBox orientation selection changed
        local iter = self:get_active_iter()
        local name = get_selected_item_name()
        if iter then local value = self.model[iter][2]
          if name then
            update_choices(name, "rotate", value, _xrandr)
            -- should find the shape item_grp (view and model) from:  his name
            _SETTINGS_CONTROLLER:modify_model_item_to_move_on_axe("rotate", value, shapes.models[name])
          end
        end
    end
    function ui.screen_symetry:on_changed()
        -- ComboBox symetry selection changed
        local iter = self:get_active_iter()
        local name = get_selected_item_name()
        if iter then local value = self.model[iter][2]
          if name then 
            _SETTINGS_CONTROLLER:modify_model_item_to_move_on_axe("reflect", value, shapes.models[name])
            update_choices(name, "reflect", value, _xrandr)
          end
        end
    end
    return setmetatable({ model = self.models, view = self.views, choices = choices, xrandr = self.xrandr }, meta)
end

-- Actions to call for control views and models 
-- of a screen setting selected (from screen name or shape mouse click)

function _SETTINGS_CONTROLLER.update_widget_screen(self)
    -- populate -**name** widget ComboBox-
    self.models.name.form:clear()
    for _, screen in pairs(self.xrandr.settings) do self.models.name.form:append({screen.name}) end
    self.views.name:update_model(self.models.name)
end

function _SETTINGS_CONTROLLER.update_widget_is_primary(self, name)
    -- from display **name** selected,
    -- => populate  -**is_primary** widget Switch-
    local primary
    if choices[name] and choices[name]["primary"] ~= nil then
      primary = choices[name].primary
    else
      primary = self.xrandr.settings[name].primary
    end
    self.views.is_primary.widget:set_active(primary)
end

function _SETTINGS_CONTROLLER.update_widget_is_enable(self, name)
    -- from display **name** selected, 
    -- => populate  -**is_enable** widget Switch-
    local enable = choices[name] and choices[name].enable or self.xrandr.settings[name].enable
    self.views.is_enable.widget:set_active(enable)
end

function _SETTINGS_CONTROLLER.update_widget_size(self, name)
    -- from display **name** selected,
    -- ==> populate -**size** widget Combobox-
    self.models.size.form:clear()
    local selected_index, size
    for idx, options in ipairs(self.xrandr.options[name]) do
      self.models.size.form:append({options.size})
      if options.selected ~= "_" and not (choices[name] and choices[name].size) then
        size = options.size
        selected_index = idx - 1
      elseif choices[name] and choices[name].size and options.size == choices[name].size then
        size = choices[name].size
        selected_index = idx - 1
      end
    end
    self.views.size:update_model(self.models.size)
    if selected_index then self.views.size.widget:set_active(selected_index) end
    if size then _SETTINGS_CONTROLLER:update_widget_frequencies(name, size)
    elseif choices[name] and choices[name].size then
      _SETTINGS_CONTROLLER:update_widget_frequencies(name, choices[name].size)
    end
end

function _SETTINGS_CONTROLLER.update_widget_frequencies(self, name, size)
    -- from display **name** AND **size** selected,
    -- => populate  -**frequency** widget ComboBox-
    local model_form = self.models.frequency.form
    self.models.frequency.form:clear()
    local selected_index = false
    for _, options in ipairs(self.xrandr.options[name]) do
      if options.size == size then
        if choices[name] and choices[name].size and choices[name].frequency then
            for i, frequency in ipairs(options.freq) do
                if frequency == choices[name].frequency then selected_index = i - 1 end
            end
        elseif type(options.selected) == "number" then selected_index = options.selected - 1 end
        for _, frequency in ipairs(options.freq) do self.models.frequency.form:append({frequency}) end
      end
    end
    self.views.frequency:update_model(self.models.frequency)
    if selected_index then self.views.frequency.widget:set_active(selected_index) end
end

function _SETTINGS_CONTROLLER.update_widget_orientation(self, name)
    -- from display **name** selected, 
    -- => populate  -**orientation** widget ComboBox-
    self.models.orientation.form:clear()
    local orientation_options = { [1] = {"0 o'clock",  "normal"}, [2] = {"3 o' clock", "right"},
                                  [3] = {"6 o' clock", "inverted"},   [4] = {"9 o' clock", "left"} }
    for _, p in ipairs(orientation_options) do self.models.orientation.form:append({p[1], p[2]}) end
    self.views.orientation:update_model(self.models.orientation)
    local i
    local rotate = choices[name] and choices[name].rotate or self.xrandr.settings[name].rotate
    for idx, p in ipairs(orientation_options) do if p[2] == rotate then i = idx - 1 end end
    self.views.orientation.widget:set_active(i)
end

function _SETTINGS_CONTROLLER.update_widget_symetry(self, name)
    -- from display **name** selected, 
    -- => populate  -**symetry** widget ComboBox-
    self.models.symetry.form:clear()
    local reflection_options = { [1] = {"no",          ''},  [2] = {"verticaly", 'x'},
                                 [3] = {"horizontaly", 'y'}, [4] = {"both",      'xy'} }
    for _, p in ipairs(reflection_options) do self.models.symetry.form:append({p[1], p[2]}) end
    self.views.symetry:update_model(self.models.symetry)
    local i
    local reflect = choices[name] and choices[name].reflect or self.xrandr.settings[name].reflect
    for idx, p in ipairs(reflection_options) do if p[2] == reflect then i = idx - 1 end end
    self.views.symetry.widget:set_active(i)
end

function _SETTINGS_CONTROLLER.get_items_positons(self)
  local items_positions = {}
  for name, settings in pairs(self.xrandr.settings) do
    items_positions[name] = self.shapes:get_setting_position(name)
  end
  return items_positions
end

function _SETTINGS_CONTROLLER.get_item_position(self, name)
  local item_pos = self.shapes:get_setting_position(name)
  print("item_position found is "..item_pos)
  return item_pos
end

meta.__index = _SETTINGS_CONTROLLER

return _SETTINGS_CONTROLLER
