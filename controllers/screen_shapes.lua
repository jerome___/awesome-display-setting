----------------------------------------------
-- Controller for Display screens draw area
-- it can be instanciate
----------------------------------------------

local lgi = require 'lgi'
local Goo = lgi.GooCanvas

local _DEVICES_CONTROLLER = {}
local meta = {}

local config = require 'models.config'
local model_screen = require 'models.screen_shape'
local screen_display = require 'views.screen_shape'
local actions = require 'controllers.actions_shapes'


local function set_final_items_positions(models)
  -- Prepare items to get new corrected position to use with xrandr final command
  -- (minimal (x,y) is zero position for each concerned axis)
  local items = { x = {}, y = {} }  -- all x and all y of each items to sort them
  for _, model in pairs(models) do
    table.insert(items.x, model.position.x); table.insert(items.y, model.position.y)
  end
  table.sort(items.x); table.sort(items.y)  -- lower first
  local items_pos = {}
  for n, model in pairs(models) do
    -- get the lower x and y and offset initial-next position on each item
    local pos_x = (model.position.x - items.x[1]) / model.scale
    local pos_y = (model.position.y - items.y[1]) / model.scale
    items_pos[n] = "+"..pos_x.."+"..pos_y
    print("        "..n.." position: ("..model.position.x..","..model.position.y..")")
  end
  return items_pos
end


function _DEVICES_CONTROLLER.build(self, canvas, xrandr, cbk_released)
  -- canvas_widget is the widget container to be parent for items to build
  -- xrandr is an instance of xrandr extractor model to use to get screen's sttings and options
  -- cbk_released is the callback function to call next after to mouse button released
  self.views = {}
  self.models = {}
  self.cbk = cbk_released
  self.root_model = Goo.CanvasGroupModel{ x = 0, y = 0,
                                          width = config.width, height = config.height}
  canvas:set_root_item_model(self.root_model)
  for name, item_settings in pairs(xrandr.settings) do
    self.models[name], self.views[name] = _DEVICES_CONTROLLER:draw_item(name, item_settings, canvas)
    actions:movable(self.views[name].item,
                    self.cbk, set_final_items_positions,
                    name, self.models)
    actions:update_item_position(name, item_settings.position)
  end
  return setmetatable( { models = self.models, views = self.views, canvas = canvas }, meta)
end

function _DEVICES_CONTROLLER.draw_item(self, name, item_settings, canvas)
  local model = model_screen:new(name, item_settings, 0.1, self.root_model)
  local view = screen_display:new(model, canvas)
  return model, view
end

function _DEVICES_CONTROLLER.get_setting_position(self, name)
  return actions.items_positions[name]
end

function _DEVICES_CONTROLLER.get_position(self, name)
  local x = actions.items_positions[name]:match("^%+(%d+)")
  local y = actions.items_positions[name]:match("^%+%d+%+(%d+)") or
            actions.items_positions[name]:match("^%+%d+%.0%+(%d+).*")
  local position = { x = x, y = y }
  return position
end

meta.__index = _DEVICES_CONTROLLER

return _DEVICES_CONTROLLER