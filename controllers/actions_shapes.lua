------------------------------------------
-- Controller for Actions on item shapes
-- Module to use to move items
-- and care about collision whith other
------------------------------------------

local config = require 'models.config'

local _ACTIONS = {}
local meta = {}

-- bound limits to never cross GooCanvas parent container's edges sides
local bound = function(posit,mini,maxi) return math.min(math.max(posit,mini),maxi) end

function _ACTIONS.movable(self, item, callback_released, callback_moving, name, models)
  -- GooCanvas item (rectangle and text for a screen) can move without overlap any other
  local dragging, offset = false, { x = 0, y = 0 }
  self.items_positions = {}      -- to use for define xrandr final setup

  local function do_collid(object1, object2) -- check if collision is happening
    return object1.x < (object2.x + object2.width) and
           (object1.x + object1.width) > object2.x and
           object1.y < (object2.y + object2.height) and
           (object1.y + object1.height) > object2.y
  end
  local function can_move(t, e) -- only if not overlap an other item
    local next_position = { x      = math.floor(e.x_root - offset.x),
                            y      = math.floor(e.y_root - offset.y),
                            width  = models[name].size.width,
                            height = models[name].size.height }
    for n, model in pairs(models) do
        if n ~= name then -- do not test with my own item model
          local object2 = { width = model.size.width, height = model.size.height,
                            x = model.position.x, y = model.position.y }
          if do_collid(next_position, object2) then return false end
        end
    end
    return true
  end
  local function flip(t, e, which)
  end
  local function want_rotate(t, e, side)
  end
  local function can_rotate(t, e, side)
  end
  local function rotate(t, e, side)
  end
  local function move(t, e)  -- let's move, but inside the GooCanvas only
    models[name]:set_position( bound(math.floor(e.x_root - offset.x), 0, config.canvas.width),
                               bound(math.floor(e.y_root - offset.y), 0, config.canvas.height) )
    self.items_positions = callback_moving(models)
  end
  function item:on_button_release_event(t, e)  -- mouse release button
    if e.button == 1 then  -- left button
      dragging = false     -- is no more draggable
      callback_released(item, t, e, name)
      for _, model in pairs(models) do  -- change border shape color
        model.rectangle.stroke_color = model.name ~= name and
                          config.canvas.color.unselected_border or
                          config.canvas.color.selected_border
      end
    end
  end
  function item:on_button_press_event(t, e) -- left mouse button pressed on item
    if e.button == 1 then  --left button
      dragging = true  -- as long as button poressed, it is draggable
      item:raise()
      -- offset (x,y) is relative mouse pointer position from top left item's corner
      -- item (x,y) is item top left corner position
      -- e (x_root, y_root) is absolute mouse pointer position
      offset = { x = math.floor(e.x_root - item.x), y = math.floor(e.y_root - item.y) }
      print("Mouse button pressed inside item "..name..":")
      print("    relative mouse position: ("..offset.x..","..offset.y..")")
      print("    absolute mouse position: ("..e.x_root..","..e.y_root..")")
      print("    item position (top left): ("..item.x..","..item.y..")")
    end
  end
  function item:on_motion_notify_event(t, e) -- mouse is moving
    if dragging and can_move(t, e) then move(t, e) end
  end
  return setmetatable( { items_positions = self.items_positions }, meta)
end

function _ACTIONS.update_item_position(self, name, position)
    self.items_positions[name] = position
end

meta.__index = _ACTIONS

return _ACTIONS