-----------------------------------------------------------
-- Test settings Module to build an object dedicated for
-- test on ui.screen_test Gtk.Button clicked evens
-----------------------------------------------------------

local lgi = require 'lgi'
local Gtk = lgi.Gtk
local GLib = lgi.GLib

local _TEST_SETTINGS = {}
local meta = {}
local ui
local settings = {}


local function  build_xrandr_cmd(items_settings)
  local cmd = "xrandr"
  for name, setting in pairs(items_settings) do
    cmd = cmd.." --output "..name
    for k, value in pairs(setting) do
      if k == "enable" and value == false then cmd = cmd.." --off" end
      if k == "primary" and value == true then cmd = cmd.." --primary" end
      if k == "size" then cmd = cmd.." --mode "..value end
      if k == "frequency" then cmd = cmd.." --rate "..value end
      if k == "position" then cmd = cmd.." --pos "..value end
      if k == "rotate" and value ~= '' then cmd = cmd.." --rotate "..value end
      if k == "reflect"  and value  ~= '' then cmd = cmd.." --reflect "..value end
    end
  end
  return cmd
end

local function manage_settings()  -- separate initial settings to nnew update on items
    local cmd_initial = {}          -- will have initial xrandr command for items (to call back if new update failed)
    local cmd_to_update = {}        -- will have xrandr command for items to update
    local xrandr = settings.xrandr
    local to_update = settings.choices
    cmd_initial = build_xrandr_cmd(xrandr.settings)
    for name, setting in pairs(xrandr.settings) do
      if to_update[name] == nil then to_update[name] = setting
      else
        for k, v in pairs(setting) do
          if to_update[name][k] == nil then to_update[name][k] = v end
        end
        local item_position = settings:get_item_position(name)
        print("new position of item "..name.." is "..item_position)
        to_update[name]["position"] = item_position
      end
    end
    cmd_to_update = build_xrandr_cmd(to_update)
    print("Done for initial command with:\n"..cmd_initial)
    print("Done for update command with:\n"..cmd_to_update)
end

local function open_dialog_timer(init_label)
    manage_settings()
    local dialog = Gtk.Dialog {
        title = "Test current settings",
        transient_for = ui.window,
        modal = true,
        buttons = { { Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL },
                    { Gtk.STOCK_OK, Gtk.ResponseType.OK }, }
    }
    local label = Gtk.Label { id = "lbl", label = init_label }
    local content = dialog:get_content_area()
    content:add(label)
    dialog:show_all()
    function dialog:on_response(id)
      if id == Gtk.ResponseType.CANCEL then dialog:destroy()
      elseif id == Gtk.ResponseType.OK then dialog:destroy() end
    end
    return dialog
end

function _TEST_SETTINGS.new(self, ui_setup, screen_settings, count)
    ui = ui_setup
    settings = screen_settings
    function ui.screen_test:on_clicked()
      -- Button *screen_test* ID clicked
      local update_label = coroutine.create(
        function() --? refresh label text value with value
          for counter = count,1,-1 do
            coroutine.yield("Does test succeed ?\nElse, stated back in "..counter.." seconds.")
          end
        end )
      local _, txt = coroutine.resume(update_label) --? first call to init label text
      local dialog = open_dialog_timer(txt)
      local label = dialog.child.lbl
      local function forward_counter() --? forward count and set label text or destroy dialog box
        local running, text = coroutine.resume(update_label)
        if text and running then label:set_text(text) else dialog:destroy() end
        return running
      end
      GLib.timeout_add_seconds(GLib.PRIORITY_DEFAULT, 1, forward_counter) -- each second as long as return true
    end
    return setmetatable( { }, meta)
end

function _TEST_SETTINGS.try_settings(self)
end

meta.__index = _TEST_SETTINGS

return _TEST_SETTINGS
